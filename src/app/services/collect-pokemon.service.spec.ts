import { TestBed } from '@angular/core/testing';

import { CollectPokemonService } from './collect-pokemon.service';

describe('CollectPokemonService', () => {
  let service: CollectPokemonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CollectPokemonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
