import { TestBed } from '@angular/core/testing';

import { DisplayPokemonCatalogueService } from './display-pokemon-catalogue.service';

describe('DisplayPokemonCatalogueService', () => {
  let service: DisplayPokemonCatalogueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DisplayPokemonCatalogueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
