import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import {environment} from 'src/environments/environment'

// variable 
const {apiURL, apiKey} = environment;

@Injectable({
  providedIn: 'root'
})

// Remove pokemon
export class RemovePokemonService {

  constructor(private http: HttpClient) { }

  public removePokemon(userId: number, pokemons: Pokemon[], pokemon: Pokemon): Observable<User>{

    const headers = new HttpHeaders( {

      "content-type": "application/json",
      "x-api-key": apiKey,
      }
    );

    return this.http.patch<User>(apiURL + "/" +userId,{
      pokemon: [...this.remove(pokemons, pokemon)]
    }, {headers})

  }

  private remove(pokemons: Pokemon[], pokemon : Pokemon): Pokemon []{
 for (let index = 0; index < pokemons.length; index++) {
      if(pokemons[index] === pokemon){

        pokemons.splice(index,1)

        return pokemons;
      }
      
    }
    return pokemons;
  }
}
