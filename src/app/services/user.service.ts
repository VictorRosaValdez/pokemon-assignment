import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user.model';


// Store information over the user
@Injectable({
  providedIn: 'root'
})
export class UserService {

  // Properties
  private _user?: User;

  // Getter and setter

  get user(): User | undefined{

    return this._user;
  }

  set user(user: User | undefined){

    if(!user){
      throw new Error ("The user is undefined.");
    }

    sessionStorage.setItem("user", JSON.stringify(user));
    this._user = user;

  }
  // Storing the user
  constructor() { 

    const storedUser = sessionStorage.getItem("user");
    if(storedUser){

      // Convert it back to an object.
      this._user = JSON.parse(storedUser);
    }
  }
}
