import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';


const url = "https://pokeapi.co/api/v2/";




// User service to displays pokemons 
@Injectable({
  providedIn: 'root' //Singleton Instance
})
export class DisplayPokemonCatalogueService {


  // Properties
  public pokemon: Pokemon [] =[]

  // Dependency Injection 
  constructor(private http: HttpClient) {

   }
 /**
     * Display pokemons
     */
  public displayPokemons() {
      
  }
  // Get connection with the API
  private fetchPokemons(): void{

        // Observables
        this.http.get<PokemonResponse>(url) //<-- Generic
        .subscribe({

          next: (response: PokemonResponse) =>{
            console.log(response.results);
            this.pokemon = response.results;
          },
          error: () =>{},
          complete: () =>{}

        });
      

  }
 }
