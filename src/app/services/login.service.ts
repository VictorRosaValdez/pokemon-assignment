import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import {environment} from 'src/environments/environment'
import { User } from 'src/app/models/user.model';

// variable 
const {apiURL} = environment;


@Injectable({
  providedIn: 'root' // Singleton Instance
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public login(username: string): Observable<User | undefined>{

    // HttpClient = Observable
    return this.http.get<User[]>(apiURL + "?username=" + username)
    
    //Return an object or an undefined
    .pipe(
      map((response: User[]) => response.pop())
    )
  }

}
