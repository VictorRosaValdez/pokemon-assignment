import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { find, map, Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment'

// variable 
const {apiURL, apiKey} = environment;

@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  constructor(private http: HttpClient) { }

  public checkUser(username: string): Observable<User | undefined>{

    // HttpClient = Observable
    return this.http.get<User>(apiURL + "?username=" + username)
    
    //Return an object or an undefined
    .pipe( find( element => element.username === username)
    )
  }

  public signUp(username: string): Observable<User | undefined>{

    // Headers
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    return this.http.post<User>(
      apiURL,
      {
        username: username,
        pokemon: [],
      },
      { headers }
    );



  }

  
}
