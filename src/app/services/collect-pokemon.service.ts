import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, CollectPokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';

// Authentication to collect Pokémon
const {apiKey, apiURL} = environment

// Collect pokemon
@Injectable({
  providedIn: 'root'
})
export class CollectPokemonService {

  constructor(private http: HttpClient) { }

  collect(userId: number, collectPokemon: CollectPokemon[] ): Observable<User>{

    // Patch


    // Header
    const headers = new HttpHeaders({
      "content-type": "application/json",
      "x-api-key": apiKey,
    });
    return this.http.patch<User>(apiURL + "/" + userId, {

      pokemon: [...collectPokemon]
    }, {
      headers
    })

  }
}
