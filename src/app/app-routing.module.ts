import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAuthenticationGuard } from './guards/user-authentication.guard';
import { UserAuthenticationTrainerGuard } from './guards/user-authentication-trainer.guard';
import { LoginPage } from './pages/login/login.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { TrainerPage } from './pages/trainer/trainer.page';

const routes: Routes = [

  {
    path: "pokemon-catalogue-page",
    component: PokemonCataloguePage,
    // Implementing the guard
    canActivate: [UserAuthenticationGuard]

  },
  {
    path: "",
    component: LoginPage,
  },
  {
    path: "trainer-page",
    component: TrainerPage,
    // Implementing the guard
    canActivate: [UserAuthenticationTrainerGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
