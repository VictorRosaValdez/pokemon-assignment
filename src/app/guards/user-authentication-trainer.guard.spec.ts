import { TestBed } from '@angular/core/testing';

import { UserAuthenticationTrainerGuard } from './user-authentication-trainer.guard';

describe('UserAuthenticationTrainerGuard', () => {
  let guard: UserAuthenticationTrainerGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UserAuthenticationTrainerGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
