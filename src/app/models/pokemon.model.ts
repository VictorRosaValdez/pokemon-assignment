
// Pokemon object
export interface Pokemon{

  id: number;
  image: string;
  name: string;
  url: string;

}

export interface CollectPokemon {
  id: number;
  image: string;
}

// Response for the connection
export interface PokemonResponse{

    count: number;
    next: string;
    previous: string;
    results: [Pokemon]

}