import { Pokemon, PokemonResponse } from '../models/pokemon.model';

export interface User{

    id: number;
    username: string;
    pokemon: Pokemon[];
  
  }