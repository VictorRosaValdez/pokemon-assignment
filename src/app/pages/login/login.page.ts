import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService} from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';
import {User} from 'src/app/models/user.model'
import { SignUpService } from 'src/app/services/sign-up.service';
@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {

  // Properties
  loading: boolean = false;
  error: string = "";

  // Constructor for the services
  constructor(private loginService: LoginService,
    private userService: UserService,
    private router: Router,
    private signUpService: SignUpService) {

  }

  ngOnInit(): void {
  }

  // Method to start
  public onStartSubmit(form: NgForm) {

    this.loading = true;

    //variable
    const { username } = form.value;

    this.loginService.login(username).subscribe({


      next: (user: User | undefined) => {
        if (user === undefined) {
          this.error = "No such user";
          return;
        }

        // Store the user and redirect to catalogue page
        else{
          this.userService.user = user;
          this.router.navigateByUrl("pokemon-catalogue-page")
        }

      },
      error: () => {

      },

      complete: () => {
        this.loading = false;
      }


    });


  }

  // Method to sign up
  public onSignUpClick(form: NgForm) {

    this.loading = true;

    //variable
    const { username } = form.value;
    

    if (username === '') {
      return;
    }

    this.signUpService.checkUser(username).subscribe({
      next:(response) =>{

        if(response === undefined){

          this.signUpService.signUp(username).subscribe({

    
            next: (user: User | undefined) => {
              
                this.userService.user = user;
                this.router.navigateByUrl("pokemon-catalogue-page")
              
      
            },
            error: () => {
      
            },
      
            complete: () => {
              this.loading = false;
            }
      
      
          });

        }

      },
      error:() =>{},
      complete:() =>{},
    })

}
}
