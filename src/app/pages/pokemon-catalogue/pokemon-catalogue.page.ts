import { Component, OnInit } from '@angular/core';
import { Pokemon, PokemonResponse } from 'src/app/models/pokemon.model';
import { HttpClient } from '@angular/common/http';
import { DisplayPokemonCatalogueService } from 'src/app/services/display-pokemon-catalogue.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';

const url = environment.apiPokemonUrl
const IMAGE_URL = environment.IMAGE_URL


@Component({
  selector: 'app-pokemon-catalogue-page',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.scss']
})
export class PokemonCataloguePage implements OnInit {

  // Properties
  pokemons: Pokemon[] = [];

  // Getter and setter
  get user(): User | undefined{
    return this.userService.user
  }

  // Dependency Injection.
  constructor(private http: HttpClient,
              private userService: UserService) { 


  }


  ngOnInit(): void {

    this.fetchPokemon()
  }

  // Get images url
  private getIdAndImageUrl(url: string): string {

    const id = Number(url.split('/').filter(Boolean).pop());
    return `${IMAGE_URL}/${id}.png`;


  }

  private fetchPokemon(): void {

    // Observables. 
    this.http.get<PokemonResponse>(url) //<-- Generic

      .subscribe({
        next: (response: PokemonResponse) => {

          console.log(response);
          this.pokemons = response.results.map(pokemon => {
            return {
              ...pokemon,
              image: this.getIdAndImageUrl(pokemon.url)
            }
          });
        },
        error: () => { },
        complete: () => { }
      })

  }


}
