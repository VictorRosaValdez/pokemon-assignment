import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { nanoid } from 'nanoid';
import { PokemonCatalogueComponent } from 'src/app/components/pokemon-catalogue/pokemon-catalogue.component';
import { Pokemon, CollectPokemon, PokemonResponse } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { CollectPokemonService } from 'src/app/services/collect-pokemon.service';
import { RemovePokemonService } from 'src/app/services/remove-pokemon.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';


//
const url = environment.apiPokemonUrl
const IMAGE_URL = environment.IMAGE_URL

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.scss']
})
export class TrainerPage implements OnInit {

  // Properties
  loading: boolean = false;
  pokemons: Pokemon[] = [];
  

    // Getter and setter
    get user(): User | undefined{
      return this.userService.user
    }

  //Constructor
  constructor(private userService: UserService,
              private collectPokemonService: CollectPokemonService,
              private http: HttpClient,
              private removePokemonService: RemovePokemonService) { }

  ngOnInit(): void {


  }

  // Get images url
  private getIdAndImageUrl(url: string): string {

    const id = Number(url.split('/').filter(Boolean).pop());
    return `${IMAGE_URL}/${id}.png`;


  }

  // Remove Pokémon
  public onRemovePokemon(pokemon : Pokemon){

    
    if(!this.userService.user){
      return;
    }
    if(this.user === undefined){
      return;
    }
    if(this.user.pokemon === undefined){
      this.user.pokemon = [];
    }
    this.removePokemonService.removePokemon(this.userService.user?.id, this.userService.user.pokemon, pokemon)
      .subscribe({
        next: (user: User) => {
          this.userService.user = user;
        },

        complete: () => {


        }

      })

  }



  
}
