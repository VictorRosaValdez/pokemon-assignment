import { Component, Input, OnInit } from '@angular/core';
import { CollectPokemon, Pokemon, PokemonResponse } from 'src/app/models/pokemon.model';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { CollectPokemonService } from 'src/app/services/collect-pokemon.service';
import { User } from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

const url = environment.apiPokemonUrl
const IMAGE_URL = environment.IMAGE_URL
const idPokemon = Number(url.split('/').filter(Boolean).pop());

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.scss']
})



export class PokemonCatalogueComponent implements OnInit {


  @Input()
  pokemons: Pokemon[] = [];
  constructor(private userService: UserService,
    private collectPokemonService: CollectPokemonService,
    private http: HttpClient,) { }

  ngOnInit(): void {
  }



  getSinglePokemon(pokemon: any) {
    return pokemon;
  }

  //Function to collect pokemon
  public onCollectSubmit(collectPokemon: CollectPokemon) {
    if (!this.userService.user) {
      return;
    }

    this.collectPokemonService.collect(this.userService.user?.id, [...this.userService.user.pokemon, collectPokemon])
      .subscribe({
        next: (user: User) => {
          this.userService.user = user;
        },

        complete: () => {


        }

      })

      alert("Pokémon collected 👌")

  }

  


}
